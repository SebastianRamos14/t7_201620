package taller.interfaz;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import sun.misc.Queue;

public class ArbolBinario implements IReconstructorArbol{

	public NodoArbol raiz;
	public Properties recorridos;
	 private String[] preorden;
	 private Queue<String> colaNodos;
	 private String[] inorden;
	public ArbolBinario() {
		raiz = null;
	}
	@Override
	public void cargarArchivo(String nombre) throws IOException {
		// TODO Auto-generated method stub
		recorridos = new Properties();
		  recorridos.load(new FileInputStream(nombre));
		  preorden = recorridos.getProperty("preorden").split(",");
		  inorden = recorridos.getProperty("inorden").split(",");
		  for (int i = 0; i < preorden.length; i++) {
			colaNodos.enqueue(preorden[i]);
		  }
	}

	@Override
	public void crearArchivo(String info) throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void reconstruir() {
		// TODO Auto-generated method stub
		try {
			String valorRaiz = colaNodos.dequeue();
			raiz = new NodoArbol(valorRaiz);
			while(!colaNodos.isEmpty())
			{
				raiz.agregar(colaNodos.dequeue());
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
