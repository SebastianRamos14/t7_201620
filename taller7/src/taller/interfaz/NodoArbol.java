package taller.interfaz;



public class NodoArbol{

	public String valor;
	public NodoArbol izquierdo, derecho;
	
	public NodoArbol(String pValor)
	{
		izquierdo = null;
		derecho = null;
	}
	
	public void agregar(String valor)
	{
		if(valor.compareTo(this.valor) < 0)
		{
			if(izquierdo == null)
			{
				izquierdo = new NodoArbol(valor);
			}
			else
			{
				izquierdo.agregar(valor);
			}
		}
		else
		{
			if(derecho == null)
			{
				derecho = new NodoArbol(valor);
			}
			else
			{
				derecho.agregar(valor);
			}
		}
	}
}
